# Docker Lab

## Build image command
```bash
docker build -t nodeapp:v1 ./   
docker build -t nodeapp:v2 -f Dockerfile.dev  ./ 
```

## Run container command
```bash
docker run --name mynodeapp1 -p 3000:3000 mynodeapp

docker run -v $(pwd):/usr/src/app  --name mynodeapp2 -p 8000:3000 nodeapp:v2
```
