const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Student ID : xxxxxxx and Full Name : xxxxxxx xxxxxxxx')
})

app.listen(port, () => {
  console.log(`Now Listening On Port ${port}`)
})
